package com.example.uldayprueba;

import android.app.Activity;
import android.app.ActionBar;
import android.app.Fragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;
import android.widget.AdapterView.OnItemSelectedListener;
import android.os.Build;

public class MainActivity extends FragmentActivity {

	Button web;
	Button go;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
  
		web=(Button) findViewById(R.id.button1);
		go=(Button) findViewById(R.id.button2);
		
		Spinner spn= (Spinner) findViewById(R.id.spinner1);
		
		ArrayAdapter adapter= ArrayAdapter.createFromResource(this,R.array.Versiones, android.R.layout.simple_spinner_item);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spn.setAdapter(adapter);
		
		spn.setOnItemSelectedListener(new OnItemSelectedListener() {
           
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, 
            		int position, long id) {
            	
            	Toast.makeText(parentView.getContext(), "Has seleccionado " +
            	          parentView.getItemAtPosition(position).toString(), Toast.LENGTH_LONG).show();
            	    
            }
                                 
            public void onNothingSelected(AdapterView<?> parentView) {
            	
            }
        });        
	
		web.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				buscador();
			}
		});
		
		go.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				ir();
			}
		});
		
		
	}

	public void buscador(){
		Intent intent = new Intent (Intent.ACTION_VIEW,Uri.parse("https://www.google.com.mx/"));
		startActivity(intent);
	}
	
	public void ir(){
		Intent variable = new Intent(MainActivity.this,OtraActividad.class);
		startActivity(variable);
		
	}
	  
	
			
			
		

			
			
		
	
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	
	

}
